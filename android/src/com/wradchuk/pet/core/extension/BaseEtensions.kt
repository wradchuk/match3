package com.wradchuk.pet.core.extension

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.loaders.ModelLoader
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader
import com.badlogic.gdx.math.Vector2
import com.wradchuk.pet.core.TEG

fun Any?.debug() {
    Gdx.app.log(TEG, this.toString())
}
fun Vector2.debug() {
    Gdx.app.log(TEG, "{ ${this.x} | ${this.y} }")
}
fun IntArray.debug() {
    Gdx.app.log(TEG, "{ ${this[0]} | ${this[0]} }")
}


fun String.createFonts(): BitmapFont {
    val font: BitmapFont
    val FONT_CHARACTERS =
        "абвгдеёжзийклмнопрстуфхцчшщъыьэюя abcdefghijklmnopqrstuvwxyz АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890.:,;'\"(!?)+-*/="
    val fontFile = Gdx.files.internal(this)
    val generator = FreeTypeFontGenerator(fontFile)
    val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
    parameter.characters = FONT_CHARACTERS
    parameter.size = 22
    parameter.color.add(Color.BLACK)
    font = generator.generateFont(parameter)
    generator.dispose()
    return font
}
fun String.loadModelOBJ(): Model {
    val loader: ModelLoader<*> = ObjLoader()
    Gdx.files.internal(this).path().debug()
    return loader.loadModel(Gdx.files.internal(this))
}