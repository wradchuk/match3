package com.wradchuk.pet.core.baseClasses

import com.badlogic.gdx.backends.android.AndroidApplication
import java.util.concurrent.Executors

open class PatchedAndroidApplication : AndroidApplication() {
    private val exec = Executors.newSingleThreadExecutor()
    private val forcePause = Runnable {
        try {
            Thread.sleep(100)
        } catch (e: InterruptedException) {
        }
        graphics.onDrawFrame(null)
    }

    override fun onPause() {
        if (useImmersiveMode) {
            exec.submit(forcePause)
        }
        super.onPause()
    }
}