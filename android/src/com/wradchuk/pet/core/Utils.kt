package com.wradchuk.pet.core

import com.badlogic.gdx.Gdx

object Utils {

    val FPS: Int get() = Gdx.graphics.framesPerSecond
    val DELTA_TIME: Float get() = Gdx.graphics.deltaTime
}