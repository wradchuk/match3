package com.wradchuk.pet

import android.content.Intent
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Vector3
import com.wradchuk.pet.core.Utils.FPS
import com.wradchuk.pet.game.GameAdapter
import com.wradchuk.pet.game.GameObject
import com.wradchuk.pet.core.baseClasses.PatchedAndroidApplication
import com.wradchuk.pet.core.extension.debug
import com.wradchuk.pet.core.extension.loadModelOBJ

class Menu( // Контекст в котором выполнять
    var context: PatchedAndroidApplication
) : InputProcessor {
    var adapter // Контроллеры и окружение
            : GameAdapter = GameAdapter(1.44338E-4f, 21.506676f, 4.508793f)
    var position_set = Vector3() // Координаты мышки сейчас в 3D
    var mouse_sy = 0 // Текушие координаты миши
    var mouse_py = 0 // Предыдущие координаты мыши
    val ALL_LEVEL = 30 // Сколько будет уровней
    val ALL_MODEL = 6 // Для теста, потом будет равно числу уровней
    var models = arrayOfNulls<Model>(ALL_MODEL) // Нашы модели уровней
    var objects = arrayOfNulls<GameObject>(ALL_LEVEL) // хранит позиции и маштабы + саму модель
    var level_pos = FloatArray(ALL_LEVEL) //
    var level_scl = FloatArray(ALL_LEVEL) //
    val DISTANCE = 3.9f // Растояние между триггерами
    val SCALE = 0.25f // Маштабирование между триггерами
    val STEP_SCALE = SCALE / DISTANCE // Шаг маштабирования
    val STEP_Z = 0.2f // Шаг смещения

    // Описание координаты триггер зоны              //
    private val z_point = floatArrayOf(
        -(DISTANCE * 2), -DISTANCE,  //
        0.0f,  //
        DISTANCE, DISTANCE * 2
    ) //

    // Указываю какой маштаб модели попавшей в зону триггера
    val s_point = floatArrayOf(1.0f, 1.25f, 1.5f, 1.25f, 1.0f) //

    //
    var modelBatch // Холст для отрисовки меню выбора уровня
            : ModelBatch
    private var selected = -1
    private var selecting = -1 // Статус выбора уровня
    private val selectionMaterial // Материал выбранного уровня
            : Material
    private val originalMaterial // Оригинальный материал уровня
            : Material
    private var isSelect = true // Выбран ли уровень
    var set_level = -1 // Какой уровень нарисовать в центре
    var visibleCount = 0

    init {
        adapter.inputMultiplexer.addProcessor(this)
        set_level = 1

        // Загрузим модели уровней
        for (i in 0 until ALL_MODEL) models[i] =
            ("model_lvl/lvl_" + (i + 1) + ".obj").loadModelOBJ()

        // Создадим модель пустышку избавляя себя от элимента с индексом 0.
        objects[0] = GameObject(models[0])
        objects[0]!!.transform.setToTranslation(-10000f, -10000f, -10000f)


        // В режиме теста установим уровням модели через рандом
        for (i in 1 until ALL_LEVEL) objects[i] = GameObject(models[adapter.rnd.nextInt(ALL_MODEL)])
        objects[set_level]!!.RCOO.z = z_point[2]
        objects[set_level]!!.RSCL = s_point[2]
        objects[set_level]!!.transform.setToTranslation(0f, 0f, objects[set_level]!!.RCOO.z)
        objects[set_level]!!.scl(objects[set_level]!!.RSCL)
        for (i in set_level - 1 downTo 1) {
            objects[i]!!.RCOO.z = DISTANCE * (i - set_level)
            objects[i]!!.RSCL = 1.25f + SCALE * (i - (set_level - 1))
            objects[i]!!.transform.setToTranslation(0f, 0f, objects[i]!!.RCOO.z)
            objects[i]!!.scl(objects[i]!!.RSCL)
        }
        for (i in set_level + 1 until ALL_LEVEL) {
            objects[i]!!.RCOO.z = DISTANCE * (i - set_level)
            objects[i]!!.RSCL = 1.5f - SCALE * (i - set_level)
            objects[i]!!.transform.setToTranslation(0f, 0f, objects[i]!!.RCOO.z)
            objects[i]!!.scl(objects[i]!!.RSCL)
        }
        for (i in 1 until ALL_LEVEL) {
            level_pos[i] = objects[i]!!.RCOO.z
            level_scl[i] = objects[i]!!.RSCL
        }
        modelBatch = ModelBatch()
        selectionMaterial = Material()
        selectionMaterial.set(ColorAttribute.createDiffuse(Color.WHITE))
        originalMaterial = Material()
    }

    fun render() {
        adapter.render()
        visibleCount = 0
        modelBatch.begin(adapter.cam)
        for (i in 1 until ALL_LEVEL) {
            if (objects[i]!!.RCOO.z > level_pos[i]) {
                objects[i]!!.RCOO.z = objects[i]!!.RCOO.z - STEP_Z
                objects[i]!!.transform.setToTranslation(0f, 0f, objects[i]!!.RCOO.z)
            }
            if (objects[i]!!.RCOO.z < level_pos[i]) {
                objects[i]!!.RCOO.z = objects[i]!!.RCOO.z + STEP_Z
                objects[i]!!.transform.setToTranslation(0f, 0f, objects[i]!!.RCOO.z)
            }
            if (objects[i]!!.RSCL > level_scl[i]) {
                objects[i]!!.RSCL = objects[i]!!.RSCL - STEP_SCALE
                objects[i]!!.scl(objects[i]!!.RSCL)
            }
            if (objects[i]!!.RSCL < level_scl[i]) {
                objects[i]!!.RSCL = objects[i]!!.RSCL + STEP_SCALE
                objects[i]!!.scl(objects[i]!!.RSCL)
            }
            if (isVisible(adapter.cam, objects[i])) {
                modelBatch.render(objects[i], adapter.environment)
                visibleCount++
            }
        }
        modelBatch.end()

        adapter.label.setText(
            "FPS: $FPS Объектов: $visibleCount Всего: ${(ALL_LEVEL - 1)} Текущий: $set_level"
        )
        adapter.stage.draw()
    }

    fun dispose() {
        adapter.dispose()
        for (i in 0..2) models[i]!!.dispose()
        modelBatch.dispose()
    }

    protected fun isVisible(cam: Camera, instance: GameObject?): Boolean {
        instance!!.transform.getTranslation(position_set)
        position_set.add(instance.center)
        return cam.frustum.sphereInFrustum(position_set, instance.radius)
    }

    fun setSelected(value: Int) {
        if (selected == value) return
        if (selected >= 0) {
            val mat = objects[selected]!!.materials[0]
            mat.clear()
            mat.set(originalMaterial)
        }
        selected = value
        if (selected >= 0) {
            val mat = objects[selected]!!.materials[0]
            originalMaterial.clear()
            originalMaterial.set(mat)
            mat.clear()
            mat.set(selectionMaterial)
        }
    }

    fun getObject(screenX: Int, screenY: Int): Int {
        val ray = adapter.cam.getPickRay(screenX.toFloat(), screenY.toFloat())
        var result = -1
        var distance = -1f
        for (i in objects.indices) {
            val instance = objects[i]
            instance!!.transform.getTranslation(position_set)
            position_set.add(instance.center)
            val dist2 = ray.origin.dst2(position_set)
            if (distance >= 0f && dist2 > distance) continue
            if (Intersector.intersectRaySphere(ray, position_set, instance.radius, null)) {
                result = i
                distance = dist2
            }
        }
        return result
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        mouse_py = context.resources.displayMetrics.heightPixels - screenY
        if (isSelect) {
            setSelected(-1)
            isSelect = false
        }
        if (getObject(screenX, screenY) > 0) {
            selecting = getObject(screenX, screenY)
            setSelected(selecting)
        }
        return selecting >= 0
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        if (selecting < 0) return false
        if (selected == selecting) {
            val ray = adapter.cam.getPickRay(screenX.toFloat(), screenY.toFloat())
            val distance = -ray.origin.y / ray.direction.y
            position_set.set(ray.direction).scl(distance).add(ray.origin)
        }
        return true
    }
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        mouse_sy = context.resources.displayMetrics.heightPixels - screenY
        if (selecting <= 0) {
            val state = scrollDir(mouse_py, mouse_sy, set_level, ALL_LEVEL)
            if (state[1] == 1) {
                if (objects[set_level]!!.menuBound(STEP_Z * 2)) {
                    set_level = state[2]
                    for (i in 1 until ALL_LEVEL) {
                        if (state[0] == 1) {
                            level_pos[i] -= DISTANCE
                            if (i < set_level) level_scl[i] -= SCALE else level_scl[i] += SCALE
                        } else {
                            level_pos[i] += DISTANCE
                            if (i <= set_level) level_scl[i] += SCALE else level_scl[i] -= SCALE
                        }
                    }
                }
            }
        }
        if (selecting >= 0) {
            try {
                context.runOnUiThread {
                    val game = Game(selecting)
                    context.startActivity(Intent(context, game.javaClass))
                    context.finish()
                }
            } catch (e: Exception) {
                e.message.debug()
            }
            setSelected(-1)
            selecting = -1
            return true
        }
        return false
    }

    /***
     * Узнаю куда крутить и можно ли крутить в этом
     * направлении, а также узнаю какой текущий уровень;
     * @param py - предыдущая координата мышки
     * @param sy - текущая координата мышки
     * @param sl - текущий уровень
     * @param al - максимальный уровень
     * @return [0] - куда крутить
     * [1] - можно ли сюда крутить
     * [2] - текущий уровень
     */
    private fun scrollDir(py: Int, sy: Int, sl: Int, al: Int): IntArray {
        val res = intArrayOf(0, 0, sl)
        if (py - sy < 0) {
            res[0] = 1
            res[2]++
        } else {
            res[0] = 0
            res[2]--
        }
        if (res[2] in 1 until al && py - sy != 0) {
            res[1] = 1
        } else {
            res[1] = 0
            res[2] = sl
        }
        return res
    }

    override fun keyDown(keycode: Int): Boolean {
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    override fun keyTyped(character: Char): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        return false
    }
}