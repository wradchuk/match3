package com.wradchuk.pet

import android.os.Bundle
import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.badlogic.gdx.graphics.GL20
import com.wradchuk.pet.game.GameAdapter
import com.wradchuk.pet.game.Level
import com.wradchuk.pet.core.baseClasses.PatchedAndroidApplication
import com.wradchuk.pet.core.extension.debug

class Game : PatchedAndroidApplication, ApplicationListener {
    var context: PatchedAndroidApplication? = null
    var adapter: GameAdapter? = null
    private var level_num = -1
    private var level: Level? = null


    constructor()
    constructor(level: Int) {
        context = this
        level_num = level
       "level_num: $level_num".debug()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = AndroidApplicationConfiguration()
        initialize(this, config)
    }

    override fun onPause() {
        super.onPause()
        Gdx.graphics.requestRendering()
    }

    override fun create() {
        WIDTH = Gdx.graphics.width
        HEIGHT = Gdx.graphics.height
        adapter = GameAdapter(8.5933134E-5f, 14.815798f, 12.46165f)


        //-------------------------------------------------------
        //  adapter.cic = new CameraInputController(adapter.cam);
        //  adapter.inputMultiplexer.addProcessor(adapter.cic);
        //--------------------------------------------------------
        Gdx.gl20.glViewport(0, 0, WIDTH, HEIGHT)
        level = Level(adapter!!, 7, 8, 8)
        adapter!!.inputMultiplexer.addProcessor(level)
    }

    override fun render() {
        Gdx.gl20.glClearColor(0.3f, 0.3f, 0.3f, 1.0f)
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)
        adapter!!.render()
        level!!.render()
        "Camera: X: ${adapter!!.cam.position.x} " +
                "Y: ${adapter!!.cam.position.y} " +
                "Z: ${adapter!!.cam.position.z}".debug()
    }

    override fun dispose() {
        level!!.dispose()
        adapter!!.dispose()
        finish()
    }

    override fun resize(width: Int, height: Int) {
        WIDTH = width
        HEIGHT = height
        Gdx.gl20.glViewport(0, 0, WIDTH, HEIGHT)
    }

    override fun pause() {}
    override fun resume() {}

    companion object {
        var WIDTH = -1
        var HEIGHT = -1
    }
}