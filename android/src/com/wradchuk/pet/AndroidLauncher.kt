package com.wradchuk.pet

import android.os.Bundle
import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.badlogic.gdx.graphics.GL20
import com.wradchuk.pet.core.Utils.FPS
import com.wradchuk.pet.game.GameAdapter
import com.wradchuk.pet.core.baseClasses.PatchedAndroidApplication

class AndroidLauncher : PatchedAndroidApplication(), ApplicationListener {
    private lateinit var adapter: GameAdapter
    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val config = AndroidApplicationConfiguration()
        initialize(this, config)

    }

    override fun onPause() {
        super.onPause()
        Gdx.graphics.requestRendering()
    }

    override fun create() {
        val metrics = context.resources.displayMetrics
        WIDTH = metrics.widthPixels
        HEIGHT = metrics.heightPixels
        adapter = GameAdapter(1.44338E-4f, 17.306675f, 4.508793f)
        menu = Menu(this)
        adapter.inputMultiplexer.addProcessor(menu)
        Gdx.gl20.glViewport(0, 0, WIDTH, HEIGHT)
    }

    override fun render() {
        Gdx.gl20.glClearColor(0.3f, 0.3f, 0.3f, 1.0f)
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        adapter.label.setText("FPS: $FPS")
        adapter.stage.draw()
        menu.render()
    }

    override fun dispose() {
        adapter.dispose()
        menu.dispose()
    }

    override fun resize(width: Int, height: Int) {
        WIDTH = width
        HEIGHT = height
        Gdx.gl20.glViewport(0, 0, WIDTH, HEIGHT)
    }

    override fun pause() {}
    override fun resume() {}

    companion object {
        var WIDTH = -1
        var HEIGHT = -1
    }
}