package com.wradchuk.pet.game

import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.wradchuk.pet.core.extension.loadModelOBJ
import java.util.Random

class Level(// Камера, окружение
    private val adapter: GameAdapter, _size_x: Int, _size_y: Int, _all_model: Int
) : InputProcessor {
    private var size_x = -1
    private var size_y = -1
    private var all_model = -1
    private var models // Загружаемые модели
            : Array<Model?>
    private var objectType // Тип объекта (моделька)
            : Array<IntArray>
    private var gameObjects // Рисуемые модели
            : Array<GameObject?>
    @JvmField
    var cord // Координаты моделей
            : Array<Vector2?>
    private var modelBatch // Где рисовать
            : ModelBatch
    private var position = Vector3() // Координаты мышки
    private var visibleCount = 0

    //----------------------------------------------------------------------------------------------
    private var selected = -1
    private var selecting = -1
    private val selectionMaterial: Material
    private val originalMaterial: Material
    private var qqq = 0
    private val M = Array(100) { IntArray(100) }
    private var zzz = true

    //----------------------------------------------------------------------------------------------
    init {
        size_x = _size_x
        size_y = _size_y
        all_model = _all_model
        models = arrayOfNulls(all_model)
        for (i in models.indices) models[i] = "model/model_$i.obj".loadModelOBJ()
        objectType = Array(size_x) { IntArray(size_y) }
        objectType = generate()
        gameObjects = arrayOfNulls(size_x * size_y + 1)
        cord = arrayOfNulls(size_x * size_y + 1)
        cord = getCord()
        gameObjects[0] = GameObject(models[0])
        cord[0] = Vector2(-10000f, -10000f)
        gameObjects[0]!!.transform.translate(cord[0]!!.x, 0f, cord[0]!!.y)
        for (i in 0 until size_x) for (j in 0 until size_y) {
            gameObjects[1 + (i + j * size_x)] = GameObject(models[objectType[i][j]])
            gameObjects[1 + (i + j * size_x)]!!.RCOO =
                Vector3(cord[1 + (i + j * size_x)]!!.x, 0f, cord[1 + (i + j * size_x)]!!.y)
            gameObjects[1 + (i + j * size_x)]!!.transform.translate(
                cord[1 + (i + j * size_x)]!!.x,
                0f,
                cord[1 + (i + j * size_x)]!!.y
            )
            gameObjects[1 + (i + j * size_x)]!!.type = objectType[i][j]
        }
        modelBatch = ModelBatch()
        selectionMaterial = Material()
        selectionMaterial.set(ColorAttribute.createDiffuse(Color.WHITE))
        originalMaterial = Material()
    }

    private fun generate(): Array<IntArray> {
        val res = Array(size_x) { IntArray(size_y) }
        val rnd = Random()

        // Генерируем
        for (i in 0 until size_x) for (j in 0 until size_y) {
            val type = rnd.nextInt(all_model)
            res[i][j] = type
        }
        return res
    }

    private fun getCord(): Array<Vector2?> {
        val res = arrayOfNulls<Vector2>(cord.size)
        res[0] = Vector2(-10000f, -10000f)
        for (i in 0 until size_x) for (j in 0 until size_y) res[1 + (i + j * size_x)] =
            Vector2((i - size_x / 2).toFloat(), j - size_y / 3.5f)
        return res
    }

    fun render() {
        //----------------------------------------------------------------------------
        qqq++
        if ((qqq / 10).toFloat() == qqq.toFloat() / 10f) {
            var k: Int
            for (y in 0 until size_y) {
                k = 1
                for (x in 1 until size_x) {
                    k = if (GO(x - 1, y)!!.type == GO(x, y)!!.type) k + 1 else 1
                    if (k >= 3) {
                        GO(x - 2, y)!!.del = true
                        GO(x - 1, y)!!.del = true
                        GO(x - 0, y)!!.del = true
                    }
                }
            }
            //-----------------------------------------------------------------------------------------
            for (x in 0 until size_x) {
                k = 1
                for (y in 1 until size_y) {
                    k = if (GO(x, y - 1)!!.type == GO(x, y)!!.type) k + 1 else 1
                    if (k >= 3) {
                        GO(x, y - 2)!!.del = true
                        GO(x, y - 1)!!.del = true
                        GO(x, y - 0)!!.del = true
                    }
                }
            }
            for (y in size_y - 1 downTo 0) for (x in 0 until size_x) if (gameObjects[1 + x + y * size_x]!!.del) vniz(
                x,
                y
            )
        }
        for (z in 0 until size_y) for (x in 0 until size_x) {
            GO(x, z)!!.RCOO = zmove(GO(x, z)!!.RCOO, GOCOO(x, z))
            GO(x, z)!!.transform.setToTranslation(GO(x, z)!!.RCOO)
        }
        //----------------------------------------------------------------------------
        visibleCount = 0
        modelBatch.begin(adapter.cam)
        for (instance in gameObjects) {
            if (isVisible(adapter.cam, instance)) {
                modelBatch.render(instance, adapter.environment)
                visibleCount++
            }
        }
        modelBatch.end()
        adapter.stage.draw()
    }

    fun dispose() {
        for (i in models.indices) models[i]!!.dispose()
        modelBatch.dispose()
        adapter.dispose()
    }

    private fun isVisible(cam: Camera, instance: GameObject?): Boolean {
        instance!!.transform.getTranslation(position)
        position.add(instance.center)
        return cam.frustum.sphereInFrustum(position, instance.radius)
    }

    private fun setSelected(value: Int) {
        if (selected == value) return
        if (selected >= 0) {
            val mat = gameObjects[selected]!!.materials[0]
            mat.clear()
            mat.set(originalMaterial)
        }
        selected = value
        if (selected >= 0) {
            val mat = gameObjects[selected]!!.materials[0]
            originalMaterial.clear()
            originalMaterial.set(mat)
            mat.clear()
            mat.set(selectionMaterial)
        }
    }

    private fun getObject(screenX: Int, screenY: Int): Int {
        val ray = adapter.cam.getPickRay(screenX.toFloat(), screenY.toFloat())
        var result = -1
        var distance = -1f
        for (i in gameObjects.indices) {
            val instance = gameObjects[i]
            instance!!.transform.getTranslation(position)
            position.add(instance.center)
            val dist2 = ray.origin.dst2(position)
            if (distance >= 0f && dist2 > distance) continue
            if (Intersector.intersectRaySphere(ray, position, instance.radius, null)) {
                result = i
                distance = dist2
            }
        }
        return result
    }

    //----------------------------------------------------------------------------------------------
    private fun In2coo(i: Int): Vector3 {
        val rez = Vector3(0f, 0f, 0f)
        rez.z = ((i - 1) / size_x).toFloat()
        rez.x = i - 1 - rez.z * size_x
        return rez
    }

    private fun GO(iNom: Int): GameObject? { // Возвращает обьект с коррдинатами x,y
        return gameObjects[iNom]
    }

    private fun GO(x: Int, y: Int): GameObject? { // Возвращает обьект с коррдинатами x,y
        return GO(1 + x + y * size_x)
    }

    private fun GOCOO(x: Int, y: Int): Vector3 { // Возвращает координат обьетка x,y
        return Vector3((x - size_x / 2).toFloat(), 0f, y - size_y / 3.5f)
    }

    private fun GOCOO(x: Float, y: Float): Vector3 { // Возвращает координат обьетка x,y
        return GOCOO(x.toInt(), y.toInt())
    }

    private fun GOCOO(coo: Vector3): Vector3 { // Возвращает координат обьетка x,y
        return GOCOO(coo.x, coo.z)
    }

    private fun GOCOO(iNom: Int): Vector3 { // Возвращает координат обьетка x,y
        return GOCOO(In2coo(iNom))
    }

    private fun rasto(Coo1: Vector3, Coo2: Vector3): Float { // Растояние между точками
        val rx = Coo2.x - Coo1.x // растояние по X
        val rz = Coo2.z - Coo1.z // растояние по Z
        return Math.sqrt((rx * rx + rz * rz).toDouble()).toFloat()
    }

    private fun zmove(Coo1: Vector3, Coo2: Vector3): Vector3 {
        Coo1.x = (Coo2.x - Coo1.x) / 10 + Coo1.x // растояние по X
        Coo1.z = (Coo2.z - Coo1.z) / 10 + Coo1.z // растояние по Z
        return Coo1
    }

    private fun vniz(x: Int, y: Int) {
        for (i in y - 1 downTo 0) {
            gameObjects[1 + x + (i + 1) * size_x] = GO(x, i)
        }
        val type = adapter.rnd.nextInt(all_model)
        val temp = GameObject(models[type])
        temp.transform.setToTranslation(Vector3(GOCOO(x, 0).x, 0f, GOCOO(x, 0).z - 3.0f))
        temp.type = type
        temp.RCOO = Vector3(GOCOO(x, 0).x, 0f, GOCOO(x, 0).z - 3.0f)
        gameObjects[1 + x] = temp
    }

    private fun getTip(mesto: Int, pos: Int, x: Int, z: Int): Int {
        var rez = -1 // По умолчанию тип не существующий
        val MX = In2coo(mesto).x.toInt()
        val MZ = In2coo(mesto).z.toInt()
        if (mesto != pos) // Если позиция не равна месту котрое смотрим
            if (MX + x >= 0 && MX + x < size_x) // Если не выходим за прежделы по X на доске
                if (MZ + z >= 0 && MZ + z < size_y) // Если не выходим за прежделы Z по  на доске
                    rez = M[MX + x][MZ + z] // Тогда возвращем ТИп
        return rez
    }

    private fun mogno(mesto: Int, pos: Int, tip: Int): Boolean {
        var rez = false
        var k = 0 // считать
        var sx: Int // Смещение по X
        var sz: Int // Смещение по Z
        for (x in 0 until size_x) for (z in 0 until size_y) M[x][z] = GO(x, z)!!.type
        val tmp = M[In2coo(mesto).x.toInt()][In2coo(mesto).z.toInt()]
        M[In2coo(mesto).x.toInt()][In2coo(mesto).z.toInt()] =
            M[In2coo(pos).x.toInt()][In2coo(pos).z.toInt()]
        M[In2coo(pos).x.toInt()][In2coo(pos).z.toInt()] = tmp
        val MCo = In2coo(mesto) // куда тащим кубик
        val PCo = In2coo(pos) // позиция того кубика котрый тащим
        if (Math.abs(MCo.x - PCo.x) <= 1) if (Math.abs(MCo.z - PCo.z) <= 1) if (Math.abs(MCo.x - PCo.x)
                .toInt() != Math.abs(MCo.z - PCo.z).toInt()
        ) {
            k = 0
            sx = -1
            while (getTip(mesto, pos, sx, 0) == tip) {
                k++
                sx = sx - 1
            }
            if (k >= 2) rez = true
            sx = 1
            while (getTip(mesto, pos, sx, 0) == tip) {
                k++
                sx = sx + 1
            }
            if (k >= 2) rez = true
            k = 0
            sz = -1
            while (getTip(mesto, pos, 0, sz) == tip) {
                k++
                sz = sz - 1
            }
            if (k >= 2) rez = true
            sz = 1
            while (getTip(mesto, pos, 0, sz) == tip) {
                k++
                sz = sz + 1
            }
            if (k >= 2) rez = true
        }
        return rez
    }

    //----------------------------------------------------------------------------------------------
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (zzz) {
            setSelected(0)
            zzz = false
        }
        if (getObject(screenX, screenY) > 0) {
            selecting = getObject(screenX, screenY)
            setSelected(selecting)
        }
        return selecting >= 0
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        if (selecting < 0) return false
        if (selected == selecting) {
            val ray = adapter.cam.getPickRay(screenX.toFloat(), screenY.toFloat())
            val distance = -ray.origin.y / ray.direction.y
            position.set(ray.direction).scl(distance).add(ray.origin)
            gameObjects[selected]!!.transform.setTranslation(position)
            //--------------------------------------------------------------------------------------
            var ex = false
            if (selected > 0) for (i in 1..size_x * size_y) if (i != selected && !ex) if (rasto(
                    GOCOO(i),
                    position
                ) < 0.2f
            ) if (mogno(i, selected, gameObjects[selected]!!.type)) {
                val temp = gameObjects[selected]
                gameObjects[selected] = gameObjects[i]
                gameObjects[i] = temp
                selected = i
                setSelected(0)
                ex = true
            }
            //--------------------------------------------------------------------------------------
        }
        return true
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (selecting >= 0) {
            setSelected(0)
            selecting = -1
            for (i in 0 until size_x) for (j in 0 until size_y) {
                GO(i, j)!!.transform.setTranslation(GOCOO(i, j))
            }
            return true
        }
        return false
    }

    //------------------------------НЕ ИСПОЛЬЗУЮ----------------------------------------------------
    override fun keyDown(keycode: Int): Boolean {
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    override fun keyTyped(character: Char): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
      return false
    }

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        return false
    } //------------------------------НЕ ИСПОЛЬЗУЮ----------------------------------------------------
}