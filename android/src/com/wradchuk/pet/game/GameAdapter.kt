package com.wradchuk.pet.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle
import com.wradchuk.pet.core.extension.createFonts
import java.util.Random

class GameAdapter(x: Float, y: Float, z: Float) {
    var spriteBatch: SpriteBatch
    var bg: Texture
    var fon: Sprite
    val ALL_BG = 2
    val IMG_NAME = "FILE.jpg"
    val IMG_PATH = Gdx.files.local(IMG_NAME).path()
    @kotlin.jvm.JvmField
    var cam: Camera
    var cic: CameraInputController? = null
    @kotlin.jvm.JvmField
    var environment: Environment
    @kotlin.jvm.JvmField
    var inputMultiplexer: InputMultiplexer
    var font: BitmapFont
    var color_text: Color
    var color_fon: Color
    var style: TextButtonStyle
    var skin: Skin
    @kotlin.jvm.JvmField
    var stage: Stage
    @kotlin.jvm.JvmField
    var label: Label
    @kotlin.jvm.JvmField
    var rnd = Random()

    init {
        inputMultiplexer = InputMultiplexer()
        Gdx.input.inputProcessor = inputMultiplexer
        environment = Environment()
        environment.set(ColorAttribute(ColorAttribute.Emissive, 0.0f, 0.0f, 0.0f, 1f))
        environment.add(DirectionalLight().set(0.8f, 0.8f, 0.8f, 0f, -1f, 0.2f))
        cam = PerspectiveCamera(45f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        cam.position[x, y] = z
        cam.lookAt(0f, 0f, 0f)
        cam.rotate(0f, 0f, 0f, 0f)
        cam.near = 1f
        cam.far = 300f
        cam.update()
        stage = Stage()
        inputMultiplexer.addProcessor(stage)
        font = "gdx/font.ttf".createFonts()
        color_text = Color(0.8f, 0.6f, 0.5f, 1f)
        color_fon = Color(0.4f, 0.4f, 0.4f, 1f)
        skin = Skin(Gdx.files.internal("gdx/uiskin.json"))
        style = TextButtonStyle()
        style.font = font
        style.fontColor = color_text
        style.up = skin.getDrawable("default-round-down")
        style.down = skin.getDrawable("default-round")
        skin.add("my-font", font, BitmapFont::class.java)
        skin.add("my-style", style, TextButtonStyle::class.java)
        label = Label("TEST", skin, "my-font", color_text)
        label.setPosition(0f, (Gdx.graphics.height - 40).toFloat())
        label.setFontScale(2f)
        stage.addActor(label)
        spriteBatch = SpriteBatch()
        bg = Texture("img/fon/bg_" + rnd.nextInt(ALL_BG) + ".png")
        fon = Sprite(bg, 0, 0, bg.width, bg.height)
        fon.setScale(2f, 2f)
        fon.regionWidth = Gdx.graphics.width
        fon.regionHeight = Gdx.graphics.height
        fon.setPosition(0f, 0f)
    }

    fun render() {
        spriteBatch.begin()
        spriteBatch.disableBlending()
        fon.draw(spriteBatch)
        spriteBatch.enableBlending()
        spriteBatch.end()
    }

    fun dispose() {}

    companion object {
        val isClick: Boolean
            get() = Gdx.input.justTouched()
        val isPressed: Boolean
            get() = Gdx.input.isTouched
    }
}