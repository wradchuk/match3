package com.wradchuk.pet.game

import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox

class GameObject(model: Model?) : ModelInstance(model) {
    @JvmField
    val center = Vector3()
    val dimens = Vector3()
    @JvmField
    var RCOO = Vector3(0f, 0f, 0f) // Где находимся в настоящий момент времени
    @JvmField
    var RSCL = 1.5f //  Начальное маштабирование объекта
    @JvmField
    var del = false
    @JvmField
    var type = 0
    @JvmField
    val radius: Float

    init {
        calculateBoundingBox(bound)
        bound.getCenter(center)
        bound.getDimensions(dimens)
        radius = dimens.len() / 4f
    }

    fun scl(scale: Float) {
        for (i in 0 until nodes.size) nodes[i].scale[scale, scale] = scale
        calculateTransforms()
    }

    fun menuBound(step: Float): Boolean {
        return RCOO.z > -step && RCOO.z < step
    }

    companion object {
        private val bound = BoundingBox()
    }
}