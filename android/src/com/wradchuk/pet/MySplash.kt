package com.wradchuk.pet

import android.os.Bundle
import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.wradchuk.pet.game.GameAdapter
import com.wradchuk.pet.core.baseClasses.PatchedAndroidApplication

class MySplash : PatchedAndroidApplication(), ApplicationListener {
    var context // Контекст в котором выполнять
            : PatchedAndroidApplication? = null
    var adapter: GameAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = AndroidApplicationConfiguration()
        initialize(this, config)
    }

    override fun onPause() {
        super.onPause()
        Gdx.graphics.requestRendering()
    }

    override fun create() {
        context = this
        adapter = GameAdapter(8.5933134E-5f, 14.815798f, 2.46165f)
    }

    override fun resize(width: Int, height: Int) {}
    override fun render() {
        adapter!!.render()
    }

    override fun pause() {}
    override fun resume() {}
    override fun dispose() {
        adapter!!.dispose()
    }
}